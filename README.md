> This is archived because using IntelliJ for code formatting on a command line isn't a good idea. Please use some library like [Spotless](https://github.com/diffplug/spotless) for code formatting instead.

# intellij-formatter

Woodpecker plugin to check formatting of IntelliJ-based profiles (or just format the project's files). Based on [sidhant92/intellij-format-action](https://github.com/sidhant92/intellij-format-action), with following changes:

* for Woodpecker systems, not GitHub Actions
* removed [reviewdog](https://github.com/reviewdog/reviewdog) support

## Usage

### Woodpecker

Example pipeline:
```yaml
pipeline:
	lint-format:
		image: qwerty287/woodpecker-intellij-formatter
		settings:
			file_pattern: *.java
			path: src/
```

### Other services with Docker

**Maybe not fully supported!**

For GitHub Actions, use [the original variant](https://github.com/sidhant92/intellij-format-action) if you don't need all the features supported by this plugin.

You can probably still use it easily if you are using Docker. Instead of `settings`, you can set environment variables starting with `PLUGIN_` followed by the settings name in capital letters (e.g. `path` would be `PLUGIN_PATH`). The entrypoint is set in the Docker image, if you need a separate one, use `/usr/local/bin/entrypoint.sh`.

### Without Docker

**Not supported!**

Run the `entrypoint.sh` script. Instead of `settings`, you can set environment variables starting with `PLUGIN_` followed by the settings name in capital letters (e.g. `path` would be `PLUGIN_PATH`).

Requirements:

* Git (only `diff` and `reset`)
* IntelliJ Idea's `idea.sh` script, must be at `/opt/idea/bin/idea.sh`
* `env-to-bool` tool (`env-to-bool.go`) in `/usr/local/bin/` (build with `go build env-to-bool.go -o /usr/local/bin/env-to-bool`)

## Settings

| Name              | Description                                                                                                                           | Default                                                                         |
|-------------------|---------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------|
| `code_style_path` | Path to code style configuration file. Relative to repository root; if file does not exist, IntelliJ's default configuration is used. | `.idea/codeStyles/Project.xml` (IntelliJ's default location for project styles) |
| `path`            | Subdirectory of repository root to format and check.                                                                                  | `.` (repository root directory)                                                 |
| `file_pattern`    | File pattern on which formatting should run                                                                                           | `*` (all files)                                                                 |
| `reset_before`    | Reset to Git's HEAD before formatting. Uncommitted changes will be lost!                                                              | `true` (reset will be performed)                                                |

## Build

To build the Docker image yourself, you can just run `make`. This requires Go to build the `env-to-bool` tool and Docker to build the image itself. Or, if you want to run the steps:

```sh
go build env-to-bool.go # build env-to-bool tool
docker build . # build Docker image
```
