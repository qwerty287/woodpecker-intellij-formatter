FROM ubuntu:20.04

COPY env-to-bool /usr/local/bin/env-to-bool
COPY entrypoint.sh /usr/local/bin/entrypoint.sh

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -yq git=1:2.25.1-1ubuntu3.5 wget=1.20.3-1ubuntu2 libfreetype6=2.10.1-2ubuntu0.2 xorg=1:7.7+19ubuntu14 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && wget --no-verbose -O /tmp/idea.tar.gz https://download.jetbrains.com/idea/ideaIC-2022.1.3.tar.gz \
    && cd /opt \
    && tar xzf /tmp/idea.tar.gz \
    && mv /opt/idea* /opt/idea \
    && rm /tmp/idea.tar.gz \
    && chmod +x /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
