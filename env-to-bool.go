package main

import (
	"os"
	"strconv"
)

func main() {
	if len(os.Args) < 2 {
		os.Exit(1)
	}
	val, ok := os.LookupEnv(os.Args[1])
	b, err := strconv.ParseBool(val)
	if !ok {
		b, err = true, nil
	}
	if err != nil {
		b = false
	}
	if b {
		os.Exit(0)
	} else {
		os.Exit(1)
	}
}
