#!/bin/bash

if /usr/local/bin/env-to-bool PLUGIN_RESET_BEFORE; then
	git reset HEAD --hard
fi

code_style_path="$(pwd)/${PLUGIN_CODE_STYLE_PATH:-".idea/codeStyles/Project.xml"}"

if [[ -z "${PLUGIN_PATH}" ]]; then
	cd "${PLUGIN_PATH}" || exit 2
fi

if test -f "$code_style_path"; then
	/opt/idea/bin/idea.sh format -s "$code_style_path" -m "${PLUGIN_FILE_PATTERN:-"*"}" -r -d .
else
	if [[ -n "${PLUGIN_CODE_STYLE_PATH}" ]]; then
		echo "[WARNING] code style configuration file does not exist"
	fi
	/opt/idea/bin/idea.sh format -m "${PLUGIN_FILE_PATTERN:-"*"}" -r -d .
fi

exit $?
